package principale;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
	private String URL;
	private String username;
	private String password;
	private String driverClassName;
	
	
	public Connexion(String uRL, String username, String password, String driverClassName) {
		super();
		URL = uRL;
		this.username = username;
		this.password = password;
		this.driverClassName = driverClassName;
	}
	
	public Connection connectToDB() {
		try {
			Connection connection =
					DriverManager.getConnection(URL, username, password);
			return connection;
			
		} catch (SQLException e) {
			
			e.printStackTrace();		
		}
		return null;
	}
	
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
	}
	public String getUsername() {
		return username;
	}
  	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPasswrd(String password) {
		this.password = password;
	}
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	

}
