package principale;

import java.sql.Connection;
import java.util.Scanner;

import entities.Admin;
import entities.Enseignant;
import entities.Etudiant;
import entities.Ue;
import services.EnseignantService;

public class Principal {
	public static void main(String[] args) {
		String URL = "jdbc:mysql://localhost:3306/gestionnotes";
		String ClassName = "con.mysql.jdbc.Driver";
		Connexion con = new Connexion(URL, "root", "",ClassName);
		Connection connexion = con.connectToDB();
		if(connexion ==null) {
			System.out.println("CONNECTION TIMED OUT");
		}else {
			System.out.println("CONNECTION ESTABILISHED");
		}
		System.out.println(" -------------------------BIENVENU SUR VOTRE LOGICIEL DE GETION DE NOTES ETUDIANTS-----------------------------");
		System.out.println("Veuillez choisir un chiffre pour continuer:");
		System.out.println("1-Plateforme administrateur\n 2-Plateforme Enseignant\n 3-Plateforme Etudiant");
		Scanner scan = new Scanner(System.in);
		int choix = scan.nextInt();
		switch(choix) {
		case 1: 
			System.out.println("1-Ajouter un nouveau etudiant\n 2-Ajouter un nouveau enseignant\n 3-Ajouter une nouvelle UE\n 4-Creer nouveau compte etudiant\n 5-Creer nouveau compte enseignant");
			int choice = scan.nextInt();
			switch(choice) {
			case 1:
				Admin admin = new Admin();
				System.out.println("--------------------------Ajout Nouveau etudiant-----------------------------------");
				Etudiant etudiant = new Etudiant();
				admin.NouveauEtudiant(etudiant);
				break;
			case 2:
				Admin adm = new Admin();
				System.out.println("----------------------------Ajout nouveau enseignant------------------------------");
				Enseignant enseignant = new Enseignant();
				adm.NouveauEnseignant(enseignant);
				break;
			case 3:
				Admin ad = new Admin();
				System.out.println("-------------------------------------- Ajout nouvelle unite d'enseignement---------------------------------");
				Ue u = new Ue();
				ad.ajouterUe(u);
				break;
			case 4:
				Admin manager = new Admin();
				System.out.println("------------------------------------Nouveau compte Etudiant--------------------------------------");
				manager.CreerCompteEtudiant();
				break;
			case 5:
				Admin manage = new Admin();
				System.out.println("------------------------------------Nouveau compte Enseignant--------------------------------------");
				manage.CreerCompteEnseignant();
		     	break;
		     	default:
		     		System.out.println("Aucune action possible");
		     		break;
			}
			break;
		case 2:
			System.out.println("1-Enregistrer note\n 2-Voir une note attribuer a un etudiant\n 3-Lire son box\n 4-Donnner suite a une reclamation");
			int answer = scan.nextInt();
			switch (answer) {
			case 1:
				Enseignant enseignant = new Enseignant();
				System.out.println("----------------------------Enregistrement de notes --------------------------------------");
				enseignant.enregistrernote();
				break;
			case 2:
				Enseignant en = new Enseignant();
				System.out.println("----------------------------------Liste des notes attribuer a un etudiant-----------------------------");
				en.VerifierNote();
				break;
			case 3:
				EnseignantService en1 = new EnseignantService();
				System.out.println("----------------------------------Vos messages -----------------------------");
			    System.out.println("Veuillez svp indiquer votre identifiant:");
			    int identifiant = scan.nextInt();
				en1.consulterInbox(identifiant);
				break;
			case 4:
				break;

			default:
				System.out.println(" Aucune action possible");
				break;
			}
			break;
		case 3:
			System.out.println("1-Consulter ses notes\n 2- Introduire une reclamation\n");
			int ans = scan.nextInt();
			switch(ans) {
			case 1:
				Etudiant et = new Etudiant();
				System.out.println("-------------------------------------UE ET NOTES-------------------------------------------");
				et.consulternote();
				break;
			case 2:
				Etudiant etudiant = new Etudiant();
				System.out.println("------------------------------SOUMISSION DE RECLAMATION-------------------------------------");
				etudiant.reclamernote();
				break;
			default:
				System.out.println("Je ne peux pas executer cette action");
				break;
			}
			break; 
		default:
			System.out.println("Je ne peux pas executer cette action");
			break;		
		}	
}
	

}
