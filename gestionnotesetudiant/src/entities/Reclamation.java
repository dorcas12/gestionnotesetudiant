package entities;

public class Reclamation {
	private Etudiant etudiant;
	private Ue u;
	private float notecontesté;
	
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public Ue getU() {
		return u;
	}
	public void setU(Ue u) {
		this.u = u;
	}
	public float getNotecontesté() {
		return notecontesté;
	}
	public void setNotecontesté(float notecontesté) {
		this.notecontesté = notecontesté;
	}
	
	

}
