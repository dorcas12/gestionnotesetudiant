package entities;

import java.util.Scanner;

import services.EnseignantService;
import services.EtudiantService;
import services.NoteService;
import services.ReclamationService;
import services.UeService;

public class Etudiant extends Personne  {
	private int numeroEtudiant;
	
	public int getNumeroEtudiant() {
		return numeroEtudiant;
	}
	
	public void setNumeroEtudiant(int numeroEtudiant) {
		this.numeroEtudiant = numeroEtudiant;
	}
	public void consulternote() {
		System.out.println("Bienvenue sur la plateforme Etudiant. Veuillez vous connecter pour continuer.");
		int numero;
		String username, password;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez renseigner le numero de votre carte d'étudiant.");
		numero = sc.nextInt();
		EtudiantService etser = new EtudiantService();
		
		System.out.println("Veuillez renseigner votre nom d'utilisateur:");
		username = sc.nextLine();
		username = sc.nextLine();
		System.out.println("Veuillez renseigner votre mot de passe:");
		password = sc.nextLine();
		
		if (etser.Authentification(numero, username, password)) {
			System.out.println("Bienvenue voici vos notes");
			NoteService noser = new NoteService();
			noser.getAll(numero);
			
		}else {
			System.out.println("!!!!!!!!!! Attention !!!!!!!!!");
		}
		
		
	}
	public void reclamernote() {
		System.out.println("Bienvenue sur la plateforme Etudiant. Veuillez vous connecter pour continuer.");
		int numero;
		String username, password;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez renseigner le numero de votre carte d'étudiant.");
		numero = sc.nextInt();
		EtudiantService etser = new EtudiantService();
		
		System.out.println("Veuillez renseigner votre nom d'utilisateur:");
		username = sc.nextLine();
		username = sc.nextLine();
		System.out.println("Veuillez renseigner votre mot de passe:");
		password = sc.nextLine();
		
		if (etser.Authentification(numero, username, password)) {
			System.out.println("Bienvenue voici Le formulaire de reclamation");
			EtudiantService etuserv = new EtudiantService();
			EnseignantService enser = new EnseignantService();
			UeService ueser = new UeService();
			ReclamationService recser = new ReclamationService();
			
			int reponse = 0;
			String valeur;
			
			do {
				String mail;
				Reclamation recl = new Reclamation();
				Etudiant etd = etuserv.getbyId(numero);
				recl.setEtudiant(etd);
				System.out.println("Renseignez l'unité d'enseignement dans laquelle vous voulez reclamer");
				valeur = sc.nextLine();
				Ue u = ueser.getByNom(valeur);
				recl.setU(u);
				System.out.println("Renseignez la note que vous contestez");
				float note = sc.nextFloat();
				recl.setNotecontesté(note);
				System.out.println("Veuille entrer un message a envoye au professeur de cette unite d'enseignement:");
				mail = sc.nextLine();
				mail = sc.nextLine();
				recser.Addreclamation(recl,enser.getEnseignantId(u.getCodeUe()),mail);
				
				System.out.println("Voulez vous soumettre une reclamation a nouveau?:");
				reponse=sc.nextInt();
				
			}while(reponse == 0);
			
		}else {
			System.out.println("!!!!!!!!!! Attention !!!!!!!!!");
		}
		
		
	}
	
}
