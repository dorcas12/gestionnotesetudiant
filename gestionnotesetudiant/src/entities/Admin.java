package entities;

import java.util.Scanner;

import javax.xml.ws.Response;

import services.ComptesService;
import services.EnseignantService;
import services.EtudiantService;
import services.UeService;

public class Admin {
	private String userName;
	private String password;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void NouveauEnseignant(Enseignant e) {
		Admin ad = new Admin();
		ad.setUserName("Admin");
		ad.setPassword("admin");
		System.out.println("Welcome veuillez vous authentifier pour continuer");
		String nomutilisateur, motpasse;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez entrer votre nom d'utilisateur");
		nomutilisateur = sc.nextLine();
		System.out.println("Veuillez entrer votre mot de passe");
		motpasse = sc.nextLine();
		if(ad.sAuthentifier(nomutilisateur, motpasse)) {
			System.out.println("Bienvenue ::::::::::::::::::"+ad.getUserName());
			int reponse = 0;
			String valeur;
			
			do {
				Enseignant enseignant = new Enseignant();
				System.out.println("Bienvenu veuillez indiquer le nom du nouveau enseignant:");
				valeur = sc.nextLine();
				enseignant.setNom(valeur);
				System.out.println("Veuillez renseigner le prenom de l'enseignant:");
				valeur = sc.nextLine();
				enseignant.setPrenom(valeur);
				System.out.println("Veuillez indiquer le sexe de l'enseignant:");
				valeur = sc.nextLine();
				enseignant.setSexe(valeur);
				System.out.println("Veuillez renseigner son age:");
				int age= sc.nextInt();
				enseignant.setAge(age);
				
				
				EnseignantService ens = new EnseignantService();
				ens.AjouterEnseignant(enseignant);
				
				System.out.println("Voulez vous coontinuer?:");
				reponse = sc.nextInt();
				
			}while(reponse==0);
		}
		
	}
	public void NouveauEtudiant(Etudiant t) {
		Admin ad = new Admin();
		ad.setUserName("Admin");
		ad.setPassword("admin");
		System.out.println("Welcome veuillez vous authentifier pour continuer");
		String nomutilisateur, motpasse;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez entrer votre nom d'utilisateur");
		nomutilisateur = sc.nextLine();
		System.out.println("Veuillez entrer votre mot de passe");
		motpasse = sc.nextLine();
		if(ad.sAuthentifier(nomutilisateur, motpasse)) {
			System.out.println("Bienvenue ::::::::::::::::::"+ad.getUserName());
			int reponse = 0,a;
			String valeur;
			
			do {
				Etudiant et = new Etudiant();
				System.out.println("Veuillez renseigner le Nom de l'etudiant");
				valeur = sc.nextLine();
				et.setNom(valeur);
				System.out.println("Veuillez renseigner le Prenom de l'etudiant");
				valeur = sc.nextLine();
				et.setPrenom(valeur);
				System.out.println("Veuillez renseigner l'age de l'etudiant");
				a = sc.nextInt();
				et.setAge(a);
				System.out.println("Veuillez renseigner le Sexe de l'etudiant");
				valeur = sc.nextLine();
				valeur = sc.nextLine();
				et.setSexe(valeur);
				EtudiantService ets = new EtudiantService();
				ets.AjouterEtudiant(et);
				System.out.println("Voulez vous continuer?");
				reponse = sc.nextInt();
				
			}while(reponse==0);
		}else {
			System.out.println("Vous n'avez pas les privileges");
		}
	
	}
	public void ajouterUe(Ue u) {
		Admin ad = new Admin();
		ad.setUserName("Admin");
		ad.setPassword("admin");
		System.out.println("Welcome veuillez vous authentifier pour continuer");
		String nomutilisateur, motpasse;
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez entrer votre nom d'utilisateur");
		nomutilisateur = sc.nextLine();
		System.out.println("Veuillez entrer votre mot de passe");
		motpasse = sc.nextLine();
		if(ad.sAuthentifier(nomutilisateur, motpasse)) {
			System.out.println("Bienvenue ::::::::::::::::::"+ad.getUserName());
			int reponse = 0;
			String valeur;
			
			do {
				Ue ue = new Ue();
				UeService us = new UeService();
				EnseignantService ens = new EnseignantService();
				System.out.println("Veuillez renseigner le code de l'unite d'enseignement:");
				valeur = sc.nextLine();
				ue.setCodeUe(valeur);
				System.out.println("Veuillez renseigner le libelle de cette UE:");
				valeur = sc.nextLine();
				ue.setLibelle(valeur);
				System.out.println("Veuillez renseigner le nombre de credits de cette UE:");
				int credit = sc.nextInt();
				ue.setCredits(credit);
				System.out.println("Veuillez renseigner le nom de l'enseignant qui sera charger de cette UE:");
				valeur = sc.nextLine();
				valeur = sc.nextLine();
				Enseignant e = ens.getEnseignantbyNom(valeur);
				System.out.println(e);
				ue.setEnseignant(e);
				
				us.AjouterUe(ue);
				System.out.println("Vulez vous enregistrer de nouveau?:");
				reponse = sc.nextInt();
				
			}while(reponse==0);
		}
	
	}
	public void CreerCompteEtudiant() {
		Admin ad = new Admin();
		ad.setUserName("Admin");
		ad.setPassword("admin");
		System.out.println("Welcome veuillez vous authentifier pour continuer:");
		String username, password;
		Scanner scan = new Scanner(System.in);
		System.out.println("Veuillez entrer votre nom d'utilisateur svp admin:");
		username = scan.nextLine();
		System.out.println("Veuillez renseigner votre mot de passe:");
		password = scan.nextLine();
		if(ad.sAuthentifier(username, password)) {
			String nomutilisateur,mdp ;
			System.out.println("OK BIENVENU ADMIN");
			System.out.println("---------------------------------Nouveau compte Etudiant----------------------------------------------------");
			EtudiantService etuser = new EtudiantService();
			etuser.getAll();
			System.out.println("Veuillez svp renseigner le numero identifiant l'etudiant:");
			int numero = scan.nextInt();
			Etudiant etudiant = etuser.getbyId(numero);
			System.out.println("Veuillez svp lui attribuer un nom d'utilisateur:");
			nomutilisateur = scan.nextLine();
			nomutilisateur = scan.nextLine();
			Comptes compte = new Comptes();
			compte.setEtudiant(etudiant);
			compte.setUserName(nomutilisateur);
			System.out.println("Veuillez lui attribuer un mot de passe:");
		    mdp = scan.nextLine();
		    mdp = scan.nextLine();
			compte.setPassword(mdp);
			ComptesService comptser = new ComptesService();
			comptser.addCompteEtudiant(etudiant, nomutilisateur,mdp);
			
		}else {
			System.out.println("Failed failed ooooooh failed la hummmmmmmm\n");
		}
	}
	public void CreerCompteEnseignant() {
		Admin ad = new Admin();
		ad.setUserName("Admin");
		ad.setPassword("admin");
		System.out.println("Welcome veuillez vous authentifier pour continuer:");
		String username, password;
		Scanner scan = new Scanner(System.in);
		System.out.println("Veuillez entrer votre nom d'utilisateur svp admin:");
		username = scan.nextLine();
		System.out.println("Veuillez renseigner votre mot de passe:");
		password = scan.nextLine();
		if(ad.sAuthentifier(username, password)) {
			System.out.println("OK BIENVENU ADMIN");
			System.out.println("---------------------------------Nouveau compte Enseignant----------------------------------------------------");
			System.out.println("Veuillez svp renseigner le nom de l'eseignant dont vous voulez creer le compte:");
			String nom = scan.nextLine();
			EnseignantService enser = new EnseignantService();
			Enseignant enseignant = enser.getEnseignantbyNom(nom);
			System.out.println("Veuillez svp lui attribuer un nom d'utilisateur:");
			String nomutilisateur = scan.nextLine();
			Comptes compte = new Comptes();
			compte.setEnseignant(enseignant);
			compte.setUserName(nomutilisateur);
			System.out.println("Veuillez lui attribuer un mot de passe:");
			String mdp = scan.nextLine();
			compte.setPassword(mdp);
			ComptesService comptser = new ComptesService();
			comptser.addCompteEnseignantt(enseignant, nomutilisateur,mdp);
			
		}else {
			System.out.println("Failed failed ooooooh failed la hummmmmmmm\n");
		}
	}
	
	public boolean sAuthentifier( String Username, String Password) {
		if(Username.equals(getUserName())&& Password.equals(getPassword())) {
			return true;
		}else {
			System.out.println("USURPATION D'IDENTITE authentification failed");
		}
		return false;
		
	
	}
	
}
