package entities;

public class Personne {
	private int id;
	private String Nom;
	private String Prenom;
	private int Age;
	private String Sexe;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}
	public String getSexe() {
		return Sexe;
	}
	public void setSexe(String sexe) {
		Sexe = sexe;
	}
	@Override
	public String toString() {
		return "User [Nom=" + Nom + ", Prenom=" + Prenom + ",Sexe=" + Sexe + "]";
	}
	

}
