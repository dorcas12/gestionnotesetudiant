package entities;

public class Notes {
	private Ue ue;
	private Etudiant etudiant;
	private float noteobtenue;
	
	
	public Ue getUe() {
		return ue;
	}
	public void setUe(Ue ue) {
		this.ue = ue;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public float getNoteobtenue() {
		return noteobtenue;
	}
	public void setNoteobtenue(float noteobtenue) {
		this.noteobtenue = noteobtenue;
	}
	
	
}
	