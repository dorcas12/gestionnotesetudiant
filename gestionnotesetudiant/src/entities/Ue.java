package entities;

public class Ue {
	private String CodeUe;
	private String Libelle;
	private int Credits;
	
	private Enseignant enseignant;
	
	public String getCodeUe() {
		return CodeUe;
	}
	public void setCodeUe(String codeUe) {
		CodeUe = codeUe;
	}
	public String getLibelle() {
		return Libelle;
	}
	public void setLibelle(String libelle) {
		Libelle = libelle;
	}
	public int getCredits() {
		return Credits;
	}
	public void setCredits(int i) {
		Credits = i;
	}
	public Enseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	@Override
	public String toString() {
		return "Ue [CodeUe=" + CodeUe + ", Libelle=" + Libelle + ", Credits=" + Credits + "]";
	}
	
	

}
