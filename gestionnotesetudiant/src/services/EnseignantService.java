package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import entities.Enseignant;
import principale.Connexion;

public class EnseignantService {
	
	public boolean AjouterEnseignant(Enseignant enseignant) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		if(connexion != null) {
			try {
			String requete = "INSERT INTO enseignants(Nom,Prenom,Age,Sexe) VALUES('"+enseignant.getNom()+"','"+enseignant.getPrenom()+"','"+enseignant.getAge()+"','"+enseignant.getSexe()+"')";
			Statement st = connexion.createStatement();
			st.executeUpdate(requete);
			return true;
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	public boolean Authentification(int id, String username, String password) { 
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement st = connexion.createStatement();
			String requete = "SELECT Username,Password FROM compte_enseignant WHERE IDEnseignant="+id;
			ResultSet res = st.executeQuery(requete);
			while(res.next()) {
				if(res.getString("Username").equals(username) && res.getString("Password").equals(password)) {
					return true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return false;
	}
	public int getEnseignantId(String referencecodeUE) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement st= connexion.createStatement();
			String sql = "SELECT Id_Enseignant FROM ue WHERE CodeUE='"+referencecodeUE+"'";
			ResultSet result = st.executeQuery(sql);
			while(result.next()) {
				int identifiant = result.getInt("Id_Enseignant");
				return identifiant;
			}
		  }catch(Exception e) {
				e.printStackTrace();
		}
		return 0;
	}
	public Enseignant getEnseignantbyNom(String nom) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement st= connexion.createStatement();
			String sql = "SELECT * FROM enseignants WHERE Nom='"+nom+"'";
			ResultSet result = st.executeQuery(sql);
			Enseignant e = null;
			while(result.next()) {
				e = new Enseignant();
				e.setId(result.getInt("Id"));
			    e.setNom(result.getString("Nom"));
			    e.setPrenom(result.getString("Prenom"));
			    e.setSexe(result.getString("Sexe"));
				return e;
			}
		  }catch(Exception e) {
				e.printStackTrace();
		}
		return null;
	}
	public void consulterInbox(int identifiant) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement st= connexion.createStatement();
			String sql = "SELECT Contenu from inbox WHERE Id_Enseignant="+identifiant;
			ResultSet result = st.executeQuery(sql);
			while(result.next()) {
				String message = result.getString("Contenu");
				System.out.println(message);
				
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
