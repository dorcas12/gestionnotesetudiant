package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import entities.Etudiant;
import principale.Connexion;

public class EtudiantService {
	
	public boolean AjouterEtudiant(Etudiant e) {
		
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		//controler l'instance de la connection
		
		if (connexion != null) {
			
			//en cas d'erreur on recuperera le message d'erreur grace a la procedure try catch
			
			try {
				Statement st = connexion.createStatement();
				String requete ="INSERT INTO etudiants(Nom,Prenom,Age,Sexe) VALUES('"+e.getNom()+"','"+e.getPrenom()+"','"+e.getAge()+"','"+e.getSexe()+"')";
				
				//executer la requette
				st.executeUpdate(requete);
				System.out.println(requete);
				System.out.println("INSERTED SUCCESSFULL");
				return true;
			}catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}
	public Etudiant getAll() {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement stat = connexion.createStatement();
			String sql = "SELECT * FROM etudiants";
			ResultSet donnees = stat.executeQuery(sql);
			Etudiant et = null;
			while(donnees.next()) {
				et = new Etudiant();
				et.setNumeroEtudiant(donnees.getInt("NumeroEtudiant"));
				et.setNom(donnees.getString("Nom"));
				et.setPrenom(donnees.getString("Prenom"));
				System.out.println(et);
				
			}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
		}
		return null;	
	}
	
	public Etudiant getbyId(int id) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement stat = connexion.createStatement();
			String sql = "SELECT NumeroEtudiant,Nom,Prenom,Sexe FROM etudiants WHERE NumeroEtudiant ="+id;
			ResultSet donnees = stat.executeQuery(sql);
			Etudiant et = null;
			while(donnees.next()) {
				et = new Etudiant();
				et.setNumeroEtudiant(donnees.getInt("NumeroEtudiant"));
				et.setNom(donnees.getString("Nom"));
				et.setPrenom(donnees.getString("Prenom"));
				et.setSexe(donnees.getString("Sexe"));
				
			}
			return et;
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
		}
		return null;
		
	}
	
	public boolean Authentification(int id, String username, String password) { 
		
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnotes" , "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		
		try {
			Statement st = connexion.createStatement();
			String requete = "SELECT Username, Password FROM compte_etudiant WHERE NumEtudiant="+id;
			ResultSet res = st.executeQuery(requete);
			while(res.next()) {
				if(res.getString("Username").equals(username) && res.getString("Password").equals(password)){
					System.out.println("Vous etes connectez");
					return true;	
				}else {
					System.out.println("Authentification failed");
				}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return false;
	}

}
